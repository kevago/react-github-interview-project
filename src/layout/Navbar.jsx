import React, { Fragment, useState } from "react";
import { AppBar, Button, Toolbar, Typography, Fab } from "@material-ui/core";
import Divider from '@material-ui/core/Divider';
import GithubIcon from "../assets/github.svg";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withRouter, matchPath, link } from 'react-router';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import AssignmentReturnIcon from '@material-ui/icons/AssignmentReturn';
import BookmarkBorderIcon from '@material-ui/icons/BookmarkBorder';
import BookmarksIcon from '@material-ui/icons/Bookmarks';
import { green, pink } from '@material-ui/core/colors';
import SvgIcon from '@material-ui/core/SvgIcon';

const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(1)
  },
  link: {
    margin: theme.spacing(1, 1.5)
  },
  title: {
    flexGrow: 1,
    textDecoration: 'none',
    cursor: 'pointer'
  },
  floatBtn: {
    padding: '5px'
  }
}));

const Navbar = (props) => {
  const [hover, sethover] = useState(false);
  const { history, location, myFavorite } = props
  const classes = useStyles();
  const isUserPathActive = !!matchPath(location.pathname, '/user/:login');
  const goToHome = () => {
    history.push(`/`);
  };
  return (
    <AppBar position='relative' color='secondary'>
      <Container maxWidth='lg'>
        <Toolbar>
          <img src={GithubIcon} className={classes.icon} alt="GithubIcon" width="50" height="50" />
          <Typography
            variant='h6'
            color='inherit'
            noWrap
            className={classes.title}
            onClick={() => goToHome()}
          >
            GITHUB BROWSER v1.0
          </Typography>

          {myFavorite.length > 0 ?
            (
              <>

                <div className={classes.floatBtn}
                  onMouseOver={() => sethover(true)}
                  onMouseOut={() => sethover(false)}
                  size="small" color="secondary" aria-label="add"
                >
                  {hover ?
                    <Link to="/favorites">
                      <BookmarksIcon color="primary" style={{ cursor: "pointer", marginRight: 30, marginTop: 4 }} size='30px' />
                    </Link>
                    :
                    <Link to="/favorites">
                      <BookmarkBorderIcon color="black" style={{ cursor: "pointer", marginRight: 30, marginTop: 4 }} size='30px' />
                    </Link>
                  }
                </div>

                <div className={classes.floatBtn}
                  onMouseOver={() => sethover(true)}
                  onMouseOut={() => sethover(false)}
                  size="small" color="secondary" aria-label="add"
                >
                  <Fab>
                    {hover ?
                      (<HomeIcon color="secondary" style={{ cursor: "pointer" }} onClick={() => history.goBack()} size='30px' />)
                      :
                      (<HomeIcon color="primary" style={{ cursor: "pointer" }} onClick={() => history.goBack()} size='30px' />)
                    }
                  </Fab>
                </div>
              </>
            )
            :
            (<>
              <div className={classes.floatBtn}
                onMouseOver={() => sethover(true)}
                onMouseOut={() => sethover(false)}
                size="small" color="secondary" aria-label="add"
              >
                {isUserPathActive && (
                  <Fab>
                    {hover ?
                      (<HomeIcon color="secondary" style={{ cursor: "pointer" }} onClick={() => history.goBack()} size='30px' />)
                      :
                      (<HomeIcon color="primary" style={{ cursor: "pointer" }} onClick={() => history.goBack()} size='30px' />)
                    }
                  </Fab>
                )}
              </div>
            </>)
          }
        </Toolbar>
      </Container >
    </AppBar >
  );
};

const mapStateToṔrops = state => {
  return {
    myFavorite: state.github.favoritesRepos
  }

}
export default connect(mapStateToṔrops, null)(withRouter(Navbar))

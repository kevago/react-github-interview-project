import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { GoRepo, GoStar, GoCode } from 'react-icons/go';
import StarOutlineIcon from '@material-ui/icons/Star';
import StarRateIcon from '@material-ui/icons/Stars';
import { connect } from 'react-redux';
import { setFavorite, myFavorite, deleteFavorite } from '../actions/github';
import "../index.css"

const UserItem = (props) => {

  const { repo, myFavorite } = props
  const [favorite, setFvorite] = useState(false)
  const handleSetFavorite = () => {
    props.setFavorite({
      repo
    })
  }

  const deleteFavorite = (id) => {
    props.deleteFavorite(id)
  }

  return (
    <Grid item xs={12} sm={6} md={6} align='left'>
      <Card className="card-item">
        <CardContent >
          <div className='repo-name'>
            <a href={repo.html_url} target='_blank' rel='noopener noreferrer'>
              {repo.name}
            </a>
          </div>
          {/* <p>{repo.description}</p> */}
          <section className="repo-info-btn">
            <article>
              {repo.language && (
                <span className='repo-info-extra'>
                  <GoCode className='repo-info-icon' size='1.2em' /> {repo.language}
                </span>
              )}

              {repo.stargazers_count > 0 && (
                <span className='repo-info-extra'>
                  <GoStar className='repo-info-icon' size='1.2em' />
                  {repo.stargazers_count}
                </span>
              )}

              {repo.forks > 0 && (
                <span className='repo-info-extra'>
                  <GoRepo className='repo-info-icon' size='1.2em' />
                  {repo.forks}
                </span>
              )}
            </article>
            <article>
              {
                myFavorite.length > 0 ? (
                  <>
                    {
                      myFavorite.filter(favorite => favorite["repo"].id === repo.id).length > 0 ? (
                        <div className="repo-info-div" onClick={() => { deleteFavorite(repo.id) }}>
                          <StarOutlineIcon color="error" className='repo-info-icon' size='1.2em' />
                        </div>
                      ) : (
                          <div className="repo-info-div" onClick={handleSetFavorite}>
                            <StarRateIcon color="action" className='repo-info-icon' size='1.2em' />
                          </div>
                        )
                    }
                  </>
                ) : (
                    <>
                      <div className="repo-info-div" onClick={handleSetFavorite}>
                        <StarRateIcon color="action" className='repo-info-icon' size='1.2em' />
                      </div>
                    </>
                  )
              }
            </article>
          </section>
        </CardContent>
      </Card>
    </Grid>
  );
};


const mapStateToṔrops = state => {
  return {
    myFavorite: state.github.favoritesRepos
  }
}

const mapDispatchToProps = {
  setFavorite,
  deleteFavorite
}

export default connect(mapStateToṔrops, mapDispatchToProps)(UserItem)

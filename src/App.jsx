import React, { Fragment } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// Pages Components
import Home from './pages/Home';
import User from './pages/User';
import Favorites from './pages/Favorites'
// Layout Components
import Navbar from './layout/Navbar';

// Redux Store
import { Provider } from 'react-redux';
import store from './store';

// Material UI
import CssBaseline from '@material-ui/core/CssBaseline';

import { makeStyles } from '@material-ui/core/styles';

// method to overide material ui styles
const useStyles = makeStyles(theme => ({
  cardGrid: {
    paddingTop: theme.spacing(0),
    paddingBottom: theme.spacing(3)
  },
  color: {
    primary: 'yellow'
  }
}));


const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <CssBaseline />
          <Navbar />
          <Switch>
            <Route exact path='/' component={Home} />
            <Route exact path='/search' component={Home} />
            <Route exact path='/user/:login' component={User} />
            {/* <Route exact path='/favorites/:login' component={Favorites} /> */}
            <Route exact path='/favorites' component={Favorites} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>
  );
};

export default App;

import React, { useEffect, Fragment, useState, useMemo } from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import { makeStyles } from '@material-ui/core/styles';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import { getUser, myFavorite } from '../actions/github';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import RepoItem from '../components/RepoItem';
import Spinner from '../components/Spinner';
import TextField from '@material-ui/core/TextField';
import { withWidth } from '@material-ui/core';

const useStyles = makeStyles({
  avatar: {
    margin: 10,
    width: 200,
    height: 200
  },
  userInfo: {
    marginTop: 20,
    marginBottom: 20
  },
  margin: {
    marginTop: 100,
    marginBottom: 10,
    marginRight: 10
  },
  textField: {
    width: '30%',
    color: 'red',
    background: 'primary',
    marginTop: '50px',
    marginBottom: '20px',
    '&::placeholder': {
      textOverflow: 'ellipsis !important',
      color: 'blue',
    }
  }
});


const Favorites = (props) => {
  const { myFavorite } = props

  const [query, setQuery] = useState('')
  const [filterFavorites, setFilterFavorites] = useState(myFavorite)

  useMemo(
    () => {
      const result = myFavorite.filter(favorite => {
        return `${favorite.repo.name} ${favorite.repo.id} ${favorite.repo.language}`.toLowerCase().includes(query.toLowerCase())
      }
      )
      setFilterFavorites(result)
    }, [myFavorite, query]
  )

  const classes = useStyles();

  return (
    <>
      <Fragment>
        <Grid container spacing={1} justify='center'>
          <Grid item xs={12} sm={12} align='center'>
            <TextField
              className={classes.textField}
              variant='outlined'
              required
              align='left'
              label='Search and filter bookmarked repositories'
              autoFocus
              value={query}
              onChange={e => {
                setQuery(e.target.value)
              }}
            />
          </Grid>
        </Grid>
      </Fragment>
      <Fragment>
        <Container>
          <Grid container className={classes.userInfo} spacing={4} justify='center'>
            {filterFavorites.length > 0 ? (
              filterFavorites.map(repo => (
                <RepoItem key={repo.repo.id} repo={repo.repo} />
              ))
            ) : (
                <h1 onKeyDown >YOU HAVE JUST EMPTIED THE BOOKMARKED REPOSITORIES LIST!!!</h1>
              )

            }
          </Grid>
        </Container>
      </Fragment> );
    </>)
};
const mapStateToProps = state => ({
  loading: state.github.loading,
  currentUser: state.github.currentUser,
  currentUserRepos: state.github.currentUserRepos,
  myFavorite: state.github.favoritesRepos

});

export default connect(
  mapStateToProps,
  { getUser }
)(Favorites);
